const axios = require('axios');

class Request {
    constructor(request = axios) {
        this.request = request;
    }
    get(url, config) {
        return this.request.get(url, config);
    }
    post(url, config) {
        return this.request.post(url, config);
    }
}

module.exports = Request;