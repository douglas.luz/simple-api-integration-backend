class HttpResponse {
    static ok(body = {}, statusCode = 200) {
        return {
            statusCode,
            body,
        };
    }
    static badRequest(missingParam) {
        return {
            statusCode: 400,
            body: {
                error: new InvalidParamError(missingParam)
            }
        }
    }
    static serverError() {
        return {
            statusCode: 500,
            body: {
                error: "Server Error"
            }
        }
    }
}

module.exports = HttpResponse;