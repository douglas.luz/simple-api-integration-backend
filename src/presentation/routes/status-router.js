const HttpResponse = require('../helpers/http-response');

class StatusRouter {
    route(httpRequest) {
        if (!httpRequest) {
            return HttpResponse.serverError();
        }
        return HttpResponse.ok({ message: "Hello! I'am here" });
    }
}

module.exports = StatusRouter;