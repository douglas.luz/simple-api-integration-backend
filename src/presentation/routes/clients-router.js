const HttpResponse = require('../helpers/http-response')

class ClientsRouter {
    constructor(createUserUseCase) {
        this.createUserUseCase = createUserUseCase
    }
    async route(httpRequest) {
        if (!httpRequest) {
            return HttpResponse.serverError()
        }
        return this.createUserUseCase.create(httpRequest.body);
    }
}

module.exports = ClientsRouter;