const { describe, expect } = require('@jest/globals')
const StatusRouter = require('../status-router')

describe('Status router', function () {
    test('Should return 200 if the application status is ok', () => {
        const sut = new StatusRouter();
        const httpRequest = {};
        const httpResponse = sut.route(httpRequest)
        expect(httpResponse.statusCode).toBe(200)
    });

    test('It should return 500 if o httpRequest is provided', () => {
        const sut = new StatusRouter();
        const httpResponse = sut.route()
        expect(httpResponse.statusCode).toBe(500)
    })
});