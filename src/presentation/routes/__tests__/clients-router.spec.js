const {it, describe} = require("@jest/globals");
const validClient = require("./support/client");
const ClientsRouter = require("../clients-router")
const HttpResponse = require("../../helpers/http-response");
const Client = require('../../../domain/Client');

function makeSut() {
    class CreateUserUseCaseSpy {
        async create(user) {
            this.user = user
            return HttpResponse.ok({ ...user }, 201)
        }
    }
    const createUserUseCaseSpy = new CreateUserUseCaseSpy()
    const sut = new ClientsRouter(createUserUseCaseSpy)
    return {
        sut,
        createUserUseCase: createUserUseCaseSpy
    }
}
describe("Clients Router", () => {
    it('should returns 201 created when passing a valid client', async function () {
        const { sut } = makeSut();
        const httpRequest = { body: new Client(validClient) };
        const httpResponse = await sut.route(httpRequest)
        expect(httpResponse.statusCode).toBe(201)
    });
    it('should returns the new client when successfully created', async function () {
        const { sut } = makeSut();
        const httpRequest = { body: new Client(validClient) };
        const httpResponse = await sut.route(httpRequest);
        expect(httpResponse.body).toEqual(new Client(validClient));
    });
    it('should call CreateUserUseCase with correct params', async function () {
        const { sut, createUserUseCase } = makeSut();
        const httpRequest = { body: new Client(validClient) };
        await sut.route(httpRequest);
        expect(createUserUseCase.user.name).toBe((httpRequest.body.name))
    });
})