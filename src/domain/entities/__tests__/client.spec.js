const { describe, it } = require("@jest/globals");
const Client = require("../Client");
const validClient = {
    cpf: '12345678910',
    rg: '123456789',
    name: 'Douglas de Souza Luz',
    nickname: 'Doug',
    email: 'douglas.luz@zrp.com.br',
    birth: new Date('1997-10-22'),
    phone: '47991699022',
    mobile: '47991699022',
    naturality: 'Brazilian',
    addresses: [
        {
            country: 'Brazil',
            state: 'Parana',
            neighborhood: 'Estação Velha',
            city: 'Mafra',
            street: 'Rua dos Bobos',
            number: '0',
            complement: 'Sem janelas',
            nickname: "Casa",
            zipCode: "89304005",
            default: false,
        }
    ]
}

describe("Client Domain", function () {
    it('should create a new instance of Client', function () {
        const sut = new Client(validClient);
        expect(sut.name).toEqual(validClient.name);
    });
})