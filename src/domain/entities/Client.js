const {attributes} = require("structure");

const Client = attributes(
    {
        id: Number,
        cpf: {
            type: String,
            required: true,
        },
        rg: {
            type: String,
            required: true,
        },
        name: {
            type: String,
            required: true,
        },
        nickname: {
            type: String,
            required: true,
        },
        email: {
            type: String,
            required: true,
        },
        birth: {
            type: Date,
            required: true,
        },
        phone: {
            type: String,
            required: true,
        },
        mobile: {
            type: String,
            required: true,
        },
        naturality: {
            type: String,
            required: true,
        },
        addresses: {
            type: Array
        }
    }
)(class Client {})

module.exports = Client