$cpf: String!
    $rg: String
$name: String
$nickname: String
$email: String!
    $birthDate: Date
$pho    ne: String
$mobile: String!
    $naturality: String
$addresses: [AddressInput]

addresses: {
    "country": "Brasil",
        "state": "SP",
        "city": "São Paulo",
        "neighborhood": "Jardim Estela",
        "street": "Travessa 22 de Setembro",
        "number": 22,
        "complement": "Casa 01",
        "nickname": "Casa",
        "zipCode": "04180-112",
        "default": true
}